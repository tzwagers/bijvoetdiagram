/******************************************************************************/
/*This program plots profile data on a KNMI theta_s,p,kappa diagram (TEMP).   */
/*Profile data should be supplied as an ascii-file listing for a series of    */
/*pressure levels:                                                            */
/*                                                                            */
/*HEIGHT PRESSURE TEMPERATURE MIXINGRATIO/DEWPOINT WINDDIRECTION WINDSPEED    */
/*                                                                            */
/*Pressure should be given in Pa, heights in m, temperatures in K, mixing     */
/*ratio in kg/kg, wind direction in degrees, and wind speed in m/s.           */
/*Pressure is needed for each level, but the other quantities are ignored when*/
/*they are negative, i.e, treated as "nodata".                                */
/*All curves in the Temp diagram are calculated using physical formulas       */
/*(see WMO international tables or memorandum of Rinus Scheele (KO-94-09).    */
/*The Temp diagram is plotted in png-image format using the GD graphics       */
/*library (see www.boutell.com/gd).                                           */
/******************************************************************************/

/*Author: Iwan Holleman/Siebren de Haan KNMI/Thom Zwagers KNMI*/
/*Date: September 2002*/
/*Modified: 30 January 2006 */
/*Modified: 07 January 2009 
  SdH: - flexible plotting region trhu command-line options
       - FZL 
       - ff ddd
/*Modified: 16 March 2012, 20 June 2013, 2014
  ZWG: - kleine aanpassingen voor EC Progtemps 
       - Onderste FL100
       - hodogram
       - shear
       - meebewegende lineaal      
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h> 
#include "gd.h"
#include "gdfonts.h"
#include "gdfontg.h"

/******************************************************************************/
/*Definition of Temp plot parameters.                                         */
/******************************************************************************/

#define DEF_T_NCOLS  520          /*Number of columns in Temp image.*/
#define DEF_T_NROWS  620          /*Number of rows in Temp image.*/
#define DEF_T_PRESN  15000.0      /*Minimum pressure in Temp in Pa.*/
#define DEF_T_PRESX  106000.0     /*Maximum pressure in Temp in Pa.*/
#define T_NPSTEP 50           /*Number of pressure steps in drawing of Temp.*/
#define DEF_T_THETAN 253.15       /*Minimum temperature in Temp in K.*/
#define DEF_T_THETAX 313.15       /*Maximum temperature in Temp in K.*/
#define  NPLOTP  10           /*Number of pressure levels in Temp.*/
float plotP[NPLOTP]={1000,925,850,700,600,500,400,300,200,100};
float BLplotP[NPLOTP]={1060, 1000,975,950,925,900,875,850,825,800};
#define  NPLOTT  91           /*Number of temperature levels in Temp.*/
float plotT[NPLOTT]={-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,
    -36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,
    -18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1.0,1,2,3,4,
    5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
    31,32,33,34,35,36,37,38,39,40};
#define	NPLOTW	13		/*Number of wet adibat levels in Temp */
float plotTw[NPLOTW]={-20,-15,-10,-5,0,5,10,15,20,25,30,35,40};
#define NPLOTTlab 12		/* Number of temperature labels in Temp */
float plotTlab[NPLOTTlab]={-30,-20,-10,0,10,20,30};
float plotTlabp[NPLOTTlab]={155,190,245,328,440,605,855};
#define  NPLOTR  8            /*Number of mixing ratio levels in Temp.*/
float plotR[NPLOTR]={1,2,4,7,11,16,22,29};
#define T_PRESR  100000.0     /*Reference pressure for potential temp. in Pa.*/
#define T_PREST  1000.0       /*Top pressure for dry adiabat calculation in Pa*/
#define T_KAPPA  0.2857       /*Deformation exponent of pressure and temp.*/
#define T_KTHETA 332.0        /*Temperature reference for deformation in K.*/
#define T_PGAPN  5           /*Minimum vertical gap between flags in pixels.*/
#define T_PGAPX  900          /*Maximum vertical gap in profile in pixels.*/

/******************************************************************************/
/*Definition of physical parameters.                                          */
/******************************************************************************/

#define WV_CA    (17.56881)   /*Constant in Water Vapor formula of Magnus*/
#define WV_CB    (241.8945)   /*Constant in Water Vapor formula of Magnus*/
#define WV_CE    (610.7)      /*Constant in Water Vapor formula of Magnus*/
#define R_AIR    (287.04)     /*Gasconstante for dry air in J/kgK.*/ 
#define EPSILON  (0.622)      /*Molecular-mass-ratio between water & dry-air.*/
#define G_GRAV   (9.80665)    /*Gravitational acceleration in m/s2.*/
#define CEL2KEL  (273.15)     /*Offset between Kelvin and degrees Celsius.*/
#define HEATCP   (1005.0)     /*Heat Capacity at constant pressure in J/kg/K.*/

/******************************************************************************/
/*Definition of standard parameters.                                          */
/******************************************************************************/

#define DEG2RAD    (M_PI/180.0)        /*Conversion from degrees to radians.*/
#define RAD2DEG    (180.0/M_PI)        /*Conversion from radians to degrees.*/
#define LSTRING    (128)               /*Length of all strings used.*/
#define OKAY       (0)                 /*Function returning without error.*/
#define FAIL       (100)               /*Function returning with an error.*/

/******************************************************************************/
/*Definition of Macros:                                                       */
/******************************************************************************/

#define XABS(x)    (((x)<0)?(-(x)):(x))
#define SIGN(x)    (((x)<0)?-1:1)
#define ROUND(x)   (((x)>0)?(int)((x)+0.5):(int)((x)-0.5))
#define SQUARE(x)  ((x)*(x))

/******************************************************************************/
/*Prototypes of local functions:                                              */
/******************************************************************************/

void thetas_p2plot(float theta_s,float pres,int *i,int *j);
void barb2image(gdImagePtr image,int i,int j,float ff,float dd,int size,int color);
float LatHeat(float Temp);
float e_sat(float Temp);
float mixr2Tdew(float mixr,float pres);
float Tdew2mixr(float Tdew,float pres);
float Tdew2Twet(float Tdew,float Temp,float pres);
float dTempdp(float pres,float Temp,char mode);
float TempPres(float pres,float pres0,float Temp0,char mode);
float TempPresDry(float pres,float pres0,float Temp0);
float pres2fl(float pres,float pressfc);
float fl2pres(float fl, float pressfc);
float calc_Tw(float T, float Td, float p);
float calc_virtual(float t, float td, float pres);

/* extrenal variables */
 int T_NCOLS,T_NROWS;
 float T_PRESN,T_PRESX,T_THETAN,T_THETAX;



/******************************************************************************/
/*Main program:                                                               */
/******************************************************************************/
int main(int argc, char *argv[])
{
char *profile,*pngfile,line[LSTRING],string[LSTRING],mode=0;
int endtime=0,i0,i1,i2,i3,i4,j0,j1,j2,j3,j4,n,m;
float mf; // voor float-iteraties
int white,black,grey,lgrey,dgrey,lblue,lgreen,green,dgreen,lbrown,blue,red, 
    llbrown,llgrey,purple,pink; 
float pres,theta,Tiso,T0dry,mixr,dd,ff;
FILE *fp;
gdImagePtr image;
char model[20], run[20], loc[20], valid[20]; 
char Psfc[20], sh03[20], sh06[20], sh09[20], sh13[20], sh16[20], sh19[20];
float FZL=-999.9;
float FZL2=-999.9;
int plotFT=0;
int windtab=0;
int printTitle=1;
int BLscale=0;

/*Checking command line arguments.*/

if (argc<3) {
   printf("Usage: %s <pro-file> <png-file> [-square]!\n",argv[0]);
   exit(1);
}
 T_NCOLS=DEF_T_NCOLS;          /*Number of columns in Temp image.*/
 T_NROWS=DEF_T_NROWS;           /*Number of rows in Temp image.*/
 T_PRESN=DEF_T_PRESN;       /*Minimum pressure in Temp in Pa.*/
 T_PRESX=DEF_T_PRESX;      /*Maximum pressure in Temp in Pa.*/
 T_THETAN=DEF_T_THETAN;       /*Minimum temperature in Temp in K.*/
 T_THETAX=DEF_T_THETAX;      /*Maximum temperature in Temp in K.*/


profile=argv[1];
pngfile=argv[2];
{
  int preserve=0;
  for (n=3 ; n<argc ; n++) {
   if (strcmp(argv[n],"-square")==0) mode='S';
   if (strcmp(argv[n],"-ncols")==0) T_NCOLS=atoi(argv[n+1]);
   if (strcmp(argv[n],"-nrows")==0) T_NROWS=atoi(argv[n+1]);
   if (strcmp(argv[n],"-presn")==0) T_PRESN=atof(argv[n+1]);
   if (strcmp(argv[n],"-presx")==0) T_PRESX=atof(argv[n+1]);
   if (strcmp(argv[n],"-thetan")==0) T_THETAN=atof(argv[n+1]);
   if (strcmp(argv[n],"-thetax")==0) T_THETAX=atof(argv[n+1]);   
   if (strcmp(argv[n],"-dpres")==0) {
     float dp=atof(argv[n+1]);
     int i;
     for(i=NPLOTP-1;i>=0;i--) {
        plotP[i]=T_PRESN/100. + (NPLOTP-1 - i)*dp;
        if (plotP[i]>1030.) plotP[i]=T_PRESX+100.;
    }
   }

   if (strcmp(argv[n],"-windtab")==0) windtab=1;
   if (strcmp(argv[n],"-noTitle")==0) printTitle=0;
   if (strcmp(argv[n],"-preserve")==0) {
       T_NCOLS=-999;          /*Number of columns in Temp image.*/
       T_NROWS=-999;           /*Number of rows in Temp image.*/
       T_PRESN=-999;       /*Minimum pressure in Temp in Pa.*/
       T_PRESX=-999;      /*Maximum pressure in Temp in Pa.*/
       T_THETAN=-999;       /*Minimum temperature in Temp in K.*/
       T_THETAX=-999;      /*Maximum temperature in Temp in K.*/
       preserve=1;
   }
   if (strcmp(argv[n],"-FZL")==0) FZL=-10;
   if (strcmp(argv[n],"-plotFT")==0) plotFT=1;
 }
 if (preserve) {
    printf("%i %i\n",T_NCOLS,T_NROWS);
    if (T_NCOLS<0 || T_NROWS<0 ) {
        printf("use -ncols and -nrows\n");
        exit(0);
    }
    if (T_PRESX > 0 && T_PRESN > 0) {
       if (T_THETAN==T_THETAX) { /* center around T_THETAN */
         float fac=T_NROWS/(T_PRESX-T_PRESN)
                  *(DEF_T_PRESX - DEF_T_PRESN)/DEF_T_NROWS;
         float dt=(DEF_T_THETAX - DEF_T_THETAN)/DEF_T_NCOLS;
         float t;
         T_THETAN-=0.5*dt/fac*T_NCOLS;
         T_THETAX+=0.5*dt/fac*T_NCOLS;
         printf("%f %f\n",fac,dt); 
         printf("%f %f\n", T_THETAN,T_THETAX);
       }
    }
 }
}
/*Allocation of the image.*/
{ int extra=20;

  image=gdImageCreateTrueColor(T_NCOLS+extra,T_NROWS);
}
if (image==NULL)  {
   printf("Memory for png-image could not be allocated!\n");
   exit(2);
}

/*Allocation of image colors, first color will be the background color.*/

white=gdImageColorAllocate(image,255,255,255);      
black=gdImageColorAllocate(image,0,0,0);
grey=gdImageColorAllocate(image,75,75,75);
blue=gdImageColorAllocate(image,0,0,230);
red=gdImageColorAllocate(image,255,0,0);
lblue=gdImageColorAllocate(image,128,128,255);
lgreen=gdImageColorAllocate(image,0,255,0);
green=gdImageColorAllocate(image,0,200,0);
dgreen=gdImageColorAllocate(image,0,150,0);
lbrown=gdImageColorAllocate(image,200,100,75);
dgrey=gdImageColorAllocate(image,40,40,40);
lgrey=gdImageColorAllocate(image,120,120,120);
llgrey=gdImageColorAllocate(image,190,190,190);
llbrown=gdImageColorAllocate(image,255,175,150);
purple=gdImageColorAllocate(image,255,0,255);
pink=gdImageColorAllocate(image,255,128,128);

/* background color */
gdImageFill(image,0,0,white);

/*Plotting of isobars.*/

for (m=0 ; m<NPLOTP ; m++) {
   pres=plotP[m]*100.0;
   thetas_p2plot(T_THETAN,pres,&i0,&j0);
   thetas_p2plot(T_THETAX,pres,&i1,&j1);
   gdImageLine(image,i0,j0,i1,j1,llgrey); 
}

/*Plotting of dry adiabats.*/
gdImageSetAntiAliased(image, llgrey);
for (m=0 ; m<NPLOTW ; m++) {
   T0dry=plotTw[m]+CEL2KEL;
   T0dry=TempPres(T_PREST,T_PRESR,T0dry,'s');
   pres=T_PRESN;
   theta=TempPresDry(pres,T_PREST,T0dry);
   theta=TempPres(T_PRESR,pres,theta,'s');
   thetas_p2plot(theta,pres,&i0,&j0);
   for (n=1 ; n<T_NPSTEP ; n++) {
      pres=T_PRESN+n*(T_PRESX-T_PRESN)/(T_NPSTEP-1);
      theta=TempPresDry(pres,T_PREST,T0dry);
      theta=TempPres(T_PRESR,pres,theta,'s');
      thetas_p2plot(theta,pres,&i1,&j1);
      gdImageLine(image,i0,j0,i1,j1,gdAntiAliased); 
      i0=i1;
      j0=j1;
   }
}

/*Plotting of wet adiabats.*/

for (m=0 ; m<NPLOTW ; m++) {
   theta=plotTw[m]+CEL2KEL;
   thetas_p2plot(theta,T_PRESN,&i0,&j0);
   thetas_p2plot(theta,T_PRESX,&i1,&j1);
   gdImageLine(image,i0,j0,i1,j1,llgrey); 
}

/*Plotting lines of constant mixing ratio.*/
gdImageSetAntiAliased(image, llgrey);
for (m=0 ; m<NPLOTR ; m++) {
   pres=T_PRESN;
   mixr=plotR[m]*1e-3;
   theta=mixr2Tdew(mixr,pres);
   theta=TempPres(T_PRESR,pres,theta,'s');
   thetas_p2plot(theta,pres,&i0,&j0);
   for (n=1 ; n<T_NPSTEP ; n++) {
      pres=T_PRESN+n*(T_PRESX-T_PRESN)/(T_NPSTEP-1);
      theta=mixr2Tdew(mixr,pres);
      theta=TempPres(T_PRESR,pres,theta,'s');
      thetas_p2plot(theta,pres,&i1,&j1);
      gdImageDashedLine(image,i0,j0,i1,j1,gdAntiAliased); 
      i0=i1;
      j0=j1;
   }
}

/*Plotting of isotherms.*/
gdImageSetAntiAliased(image, llgrey);
for (m=0 ; m<NPLOTT ; m++) {
   pres=T_PRESN;
   Tiso=2*plotT[m]+CEL2KEL;
   theta=TempPres(T_PRESR,pres,Tiso,'s');
   thetas_p2plot(theta,pres,&i0,&j0);
   /* thick line for 10´s */
   int ten = 2*(int)plotT[m];
   if (ten%10==0) {gdImageSetThickness(image, 2);} else {gdImageSetThickness(image, 1);}  
   for (n=1 ; n<T_NPSTEP ; n++) {
      pres=T_PRESN+n*(T_PRESX-T_PRESN)/(T_NPSTEP-1);
      theta=TempPres(T_PRESR,pres,Tiso,'s');
      thetas_p2plot(theta,pres,&i1,&j1);
      if (fabs(Tiso-CEL2KEL)<0.5) {
         gdImageSetAntiAliased(image, lblue);
         gdImageLine(image,i0,j0,i1,j1,gdAntiAliased);
         gdImageSetAntiAliased(image, lblue);
      }
      else gdImageLine(image,i0,j0,i1,j1,gdAntiAliased);
      i0=i1;
      j0=j1;
   }
}

/*Opening of TEMP file and reading date/time.*/

fp=fopen(profile,"r");
if (fp==NULL) {
   printf("Temp-file could not be opened!\n");
   exit(3);
}
while (fgets(line,sizeof(line),fp)) {
   sscanf(line,"#Vertical Profile from %s",model);
   sscanf(line,"#Profile run : %s",run);
   sscanf(line,"#Profile station : %s",loc);
   sscanf(line,"#Profile valid : %s",valid);
   sscanf(line,"#SFC Pressure: %s",Psfc);
   sscanf(line,"#SHR 0-3: %s",sh03);
   sscanf(line,"#SHR 0-6: %s",sh06);
   sscanf(line,"#SHR 0-9: %s",sh09);
   sscanf(line,"#SHR 1-3: %s",sh13);
   sscanf(line,"#SHR 1-6: %s",sh16);   
   sscanf(line,"#SHR 1-9: %s",sh19);
}

/*Plotting of TEMP data from file (Temperature, dewpoint and wet bulb).*/

rewind(fp); 
i1=i2=i3=i4=j1=j2=j3=j4=-1;
float T0=0,T1,P0,P1;

while (fgets(line,sizeof(line),fp)) {

	if (sscanf(line,"%*f %f %f %f %f %f",&pres,&Tiso,&mixr,&dd,&ff)==5) {
      	if (pres<0) continue;
      
      		/* Temperature */
      		if (Tiso>=0) {
         		theta=TempPres(T_PRESR,pres,Tiso,'s');
         		thetas_p2plot(theta,pres,&i0,&j0);
         		if (j1>=0&&XABS(j0-j1)<T_PGAPX) {
				gdImageSetThickness(image,2);
            			gdImageSetAntiAliased(image, red);
            			gdImageLine(image,i1,j1,i0,j0,gdAntiAliased);
      			}
  			i1=i0;
  			j1=j0;
  			if (FZL>-990) {
	   			T1=Tiso;
	   			P1=pres;
	   			if (T0>0 && ( (T0>CEL2KEL && T1<CEL2KEL) || ( (T1>CEL2KEL && T0<CEL2KEL) ) )) {
	     				float dt;
	     				dt=(T1-T0)/(log(P1) - log(P0));
	     				if (fabs(dt)>1.e-10) {
	       					if (FZL2>0) FZL=pres2fl(exp((273.15 - T0)/dt + log(P0)), strtof(Psfc, NULL));
	       					else FZL2=pres2fl(exp((273.15 - T0)/dt + log(P0)), strtof(Psfc, NULL));
	     				}
	     
	   			}
	   			T0=T1;
	   			P0=pres;
         		}
      		}
      
      		/* Dewpoint */
      		if (mixr>=0) {
         		//if (mixr>1.0) mixr=Tdew2mixr(mixr,pres);
         		//theta=mixr2Tdew(mixr,pres);
	 		theta=mixr;
         		theta=TempPres(T_PRESR,pres,theta,'s');
         		thetas_p2plot(theta,pres,&i0,&j0);
         		if (j2>=0&&XABS(j0-j2)<T_PGAPX){
				gdImageSetThickness(image,2); 
            			gdImageSetAntiAliased(image, blue);
            			gdImageLine(image,i2,j2,i0,j0,gdAntiAliased);
         		}
         		i2=i0;
         		j2=j0;
      		}
		
			/* Wet bulb */
			if (mixr>=0 && Tiso>=0){
				theta = calc_Tw(Tiso, mixr, pres);
				theta=TempPres(T_PRESR,pres,theta,'s');
				thetas_p2plot(theta,pres,&i0,&j0);
					if (j3>=0&&XABS(j0-j3)<T_PGAPX){ 
						gdImageSetAntiAliased(image, dgreen);
						gdImageSetThickness(image,1);
						gdImageLine(image,i3,j3,i0,j0,gdAntiAliased);
					}
					i3=i0;
					j3=j0;
			}
		
			/* Virtual temp */
			if (mixr>=0 && Tiso>=0){
				theta = calc_virtual(Tiso, mixr, pres);
				theta=TempPres(T_PRESR,pres,theta,'s');
				thetas_p2plot(theta,pres,&i0,&j0);
					if (j4>=0&&XABS(j0-j4)<T_PGAPX){ 
						gdImageSetAntiAliased(image, pink);
						gdImageSetThickness(image,0.5);
						gdImageLine(image,i4,j4,i0,j0,gdAntiAliased);
					}
					i4=i0;
					j4=j0;
			}
   	}
}

/*Erasing features outside of TEMP frame and plotting of frame.*/

gdImageSetThickness(image,2);
thetas_p2plot(T_THETAN,T_PRESX,&i0,&j0);
thetas_p2plot(T_THETAX,T_PRESN,&i1,&j1);
gdImageFilledRectangle(image,0,0,gdImageSX(image)-1,j1,white);
gdImageFilledRectangle(image,0,j1,i0,j0,white);
gdImageFilledRectangle(image,i1,j1,gdImageSX(image)-1,j0,white);
gdImageFilledRectangle(image,0,j0,gdImageSX(image)-1,gdImageSY(image)-1,white);
gdImageSetThickness(image,1);
gdImageRectangle(image,i0,j0,i1,j1,llgrey);
gdImageLine(image,i0,j0,i1,j0,llgrey);
gdImageLine(image,i0,j1,i1,j1,llgrey);

/*Hodogram*/
int k0, l0;
thetas_p2plot(T_THETAN,T_PRESX,&i0,&j0);
thetas_p2plot(T_THETAX,T_PRESN,&i1,&j1);
thetas_p2plot(283.15,300*100,&k0,&l0);
gdImageFilledRectangle(image,i0,j1,k0,l0,white);
gdImageRectangle(image,i0,j1,k0,l0,llgrey);

/*hodo center cross*/
gdImageLine(image,-3+i0+(k0-i0)/2, j1+(l0-j1)/2, 3+i0+(k0-i0)/2, j1+(l0-j1)/2, black);
gdImageLine(image, i0+(k0-i0)/2, -3+j1+(l0-j1)/2, i0+(k0-i0)/2, 3+j1+(l0-j1)/2, black);

int c;
float x1,y1,x2,y2,x3,y3,a,r;
gdImageSetThickness(image,1);
for (c=10; c<=100; c+=10){
    if (c % 50 ==0) gdImageSetAntiAliased(image, lblue); else gdImageSetAntiAliased(image, llgrey);
    r = (c/100.)*((k0-i0)/2.);
    for (a=0; a<360; a+=1){
        x1 = i0+(k0-i0)/2.+r*cos(DEG2RAD*a);
        y1 = j1+(l0-j1)/2.+r*sin(DEG2RAD*a);
        x2 = i0+(k0-i0)/2.+r*cos(DEG2RAD*(a+1));
        y2 = j1+(l0-j1)/2.+r*sin(DEG2RAD*(a+1));       
        gdImageLine(image,x1,y1,x2,y2,gdAntiAliased);
    }
}



x1=i0+(k0-i0)/2.; //for later use
y1=j1+(l0-j1)/2.; //for later use
x2=x1;
y2=y1;

/*Reading and plotting of TEMP data from file, cont'd (Wind barbs).*/
float add = 0; // afwisselend links of rechts.
j1=-1;
rewind(fp);
while (fgets(line,sizeof(line),fp)) {
   if (sscanf(line,"%*f %f %f %f %f %f",&pres,&Tiso,&mixr,&dd,&ff)==5) {
	  gdImageSetThickness(image,1); 
      if (pres<0.0||dd<0.0||ff<0.0) continue;
      if (pres<T_PRESN||pres>T_PRESX) continue;
      thetas_p2plot(T_THETAN,pres,&i0,&j0);
      if (j1>=0&&XABS(j0-j1)<T_PGAPN) continue;
      if (add==0) add = 0.055*T_NCOLS; else add = 0; // afwisselend links en rechts
      barb2image(image,0.07*T_NCOLS+add,j0,ff,dd,0.04*T_NCOLS,black);	
      if (windtab) {
        sprintf(string,"%03d%4i",(int)(10*round(dd/10)),(int)(ff/0.5144));
        gdImageString(image,gdFontSmall,0.11*T_NCOLS,j0,
                    (unsigned char *)string,black);
      }
      
      // hodogram
		 gdImageSetThickness(image,2); 
		 gdImageSetAntiAliased(image, black);
		 if (pres2fl(pres, strtof(Psfc, NULL))>0 && pres2fl(pres, strtof(Psfc, NULL))/0.3048<100){
			  gdImageSetAntiAliased(image, red);
		 }
		 if (pres2fl(pres, strtof(Psfc, NULL))/0.3048>=100 && pres2fl(pres, strtof(Psfc, NULL))/0.3048<300){
			  gdImageSetAntiAliased(image, green);
		 }
		 if (pres2fl(pres, strtof(Psfc, NULL))/0.3048>=300 && pres2fl(pres, strtof(Psfc, NULL))/0.3048<600){
			  gdImageSetAntiAliased(image, blue);
		 }
		 if (pres2fl(pres, strtof(Psfc, NULL))/0.3048>=600 && pres2fl(pres, strtof(Psfc, NULL))/0.3048<900){
			  gdImageSetAntiAliased(image, purple);
		 }
		 if (pres2fl(pres, strtof(Psfc, NULL))/0.3048>=900 && pres2fl(pres, strtof(Psfc, NULL))/0.3048<1200){
			  gdImageSetAntiAliased(image, lgrey);
		 }  
      
         x3 = x1+(ff/0.5144)*cos(DEG2RAD*(dd+90));
         y3 = y1+(ff/0.5144)*sin(DEG2RAD*(dd+90));
         if (x1!=x2 && y1!=y2) gdImageLine(image,x2,y2,x3,y3,gdAntiAliased);
         x2=x3;
         y2=y3;
      j1=j0;
   }
}
fclose(fp);

/* Texts */
int brect[8];
char *f = "ssmedium.ttf";  /* User supplied font */
double sz = 10.; /*font size in pixels*/

gdImageSetThickness(image,1);
/*Plotting of TEMP title.*/
if (printTitle) {
 thetas_p2plot((T_THETAN+T_THETAX)/2,T_PRESX,&i0,&j0);
 sprintf(string,"%s valid: %s",loc,valid);
 gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
 //i0=(T_NCOLS-(brect[2]-brect[6])*1.5);
 j0=(brect[3]-brect[7])*1.5;
 gdImageStringFT(image,&brect[0],black,f,20,0.0,i0,j0,string);
 sprintf(string,"%s%s",model,run);
 gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
 i0=0;
 j0=(brect[3]-brect[7])*1.5;
 gdImageStringFT(image,&brect[0],black,f,20,0.0,i0,j0,string);
}

/*Plotting of TEMP labels.*/
for (m=0 ; m<NPLOTP ; m++) {
   pres=plotP[m]*100.0;
   sprintf(string,"%g",plotP[m]);
   thetas_p2plot(T_THETAN,pres,&i0,&j0);
   gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
   i0-=brect[2]-brect[6];
   j0-=(brect[7]-brect[3])/2;
   gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0,j0,string);
}

/* plot temperature labels inside Temp */
for (m=0 ; m<NPLOTTlab ; m++) {
   Tiso=plotTlab[m]+CEL2KEL;
   float pLevel=plotTlabp[m]*100.0;
   sprintf(string,"%g",plotTlab[m]);
   theta=TempPres(T_PRESR,pLevel,Tiso,'s');
   theta=308.15;
   thetas_p2plot(theta,pLevel,&i0,&j0);
   gdImageStringFT(NULL,&brect[0],0,f,sz,45.0,0,0,string);
   //i0+=(brect[2]-brect[6])/2;
   j0-=(brect[3]-brect[7])/2;
   gdImageStringFT(image,&brect[0],black,f,sz,45.0,i0,j0,string);
}

/* Plot hoogtevoeten/meters als lineaal aan de rechterkant */
thetas_p2plot(T_THETAX,strtof(Psfc, NULL)*100,&i0,&j0);
thetas_p2plot(T_THETAN,T_PRESN,&i1,&j1);
gdImageLine(image,i0+20,j0,i0+20,j1,black);
gdImageLine(image,i1,j0,i0+24,j0,black);
sprintf(string,"%3i",0);
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
i0+=brect[2]-brect[6];
j0-=(brect[7]-brect[3])/2;
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0,j0,string);
//gdImageString(image,gdFontSmall,i0+26,j0-6,(unsigned char *)string,black);

/* feet */
for (m=500; m<=4500; m+=500) {
    thetas_p2plot(T_THETAX, fl2pres((float)m*0.3048, strtof(Psfc, NULL)) ,&i0,&j0);
    if (m%500==0 && m%1000!=0){
       gdImageLine(image,i0+20,j0,i0+22,j0,grey);
    }   
    if (m%1000==0){
       gdImageLine(image,i0+20,j0,i0+24,j0,black);
    }
}
for (m=5000; m<=44000; m+=1000) {
    thetas_p2plot(T_THETAX, fl2pres((float)m*0.3048, strtof(Psfc, NULL)) ,&i0,&j0);
    gdImageLine(image,i0+20,j0,i0+24,j0,black);
    if (m%5000==0){
       sprintf(string,"%3i",m/100);
       gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
       i0+=brect[2]-brect[6];
       j0-=(brect[7]-brect[3])/2;
       gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0,j0,string);
       //gdImageString(image,gdFontSmall,i0+26,j0-6,(unsigned char *)string,black);
    }
}

/* km */
for (m=100; m<=1900; m+=100) {
    thetas_p2plot(T_THETAX, fl2pres((float)m, strtof(Psfc, NULL)) ,&i0,&j0);
    gdImageLine(image,i0+18,j0,i0+20,j0,grey);
    if (m==1000){
       gdImageLine(image,i0+16,j0,i0+20,j0,black);
       sprintf(string,"%2i",m/1000);
       gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
       //i0-=(brect[2]-brect[6]);
       j0-=(brect[7]-brect[3])/2;
       gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0,j0,string);
       //gdImageString(image,gdFontSmall,i0+4,j0-6,(unsigned char *)string,black);
    }
}
for (m=2000; m<=13000; m+=1000) {
    thetas_p2plot(T_THETAX, fl2pres((float)m, strtof(Psfc, NULL)) ,&i0,&j0);
    gdImageLine(image,i0+16,j0,i0+20,j0,black);
    if (m%1000==0){
       sprintf(string,"%2i",m/1000);
       gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
       //i0-=(brect[2]-brect[6]);
       j0-=(brect[7]-brect[3])/2;
       gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0,j0,string);
       //gdImageString(image,gdFontSmall,i0+4,j0-6,(unsigned char *)string,black);
    }
}

/* mixing ratio labels */
/*
for (m=0 ; m<NPLOTR ; m++) {
   mixr=plotR[m]*1e-3;
   sprintf(string,"%g",plotR[m]);
   theta=mixr2Tdew(mixr,T_PRESX);
   thetas_p2plot(theta,T_PRESX,&i0,&j0);
   gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,string);
   i0-=(brect[2]-brect[6])/2;
   j0+=16;
   if (theta>=T_THETAN&&theta<=T_THETAX)
   gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0,j0,string);
}
*/

// hodo labels
thetas_p2plot(T_THETAN,250*100.0,&i0,&j0);
gdImageLine(image,i0,j0,i0+10,j0,red);
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+15,j0+3,"0-1");
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageLine(image,i0,j0,i0+10,j0,green);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+15,j0+3,"1-3");
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageLine(image,i0,j0,i0+10,j0,blue);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+15,j0+3,"3-6");
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageLine(image,i0,j0,i0+10,j0,purple);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+15,j0+3,"6-9");
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageLine(image,i0,j0,i0+10,j0,lgrey);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+15,j0+3,"9-12");

// textual FZL's
if (FZL>-999.0) {
  thetas_p2plot(T_THETAN,300*100.0,&i0,&j0);
  if (FZL>-10 && FZL<200) {
    sprintf(string,"FZL=%i ft",(int)(100*FZL));
    gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
    j0-=(brect[7]-brect[3]);
    gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);
  }
  if (FZL2>-10 && FZL2<200) {
    sprintf(string,"FZL=%i ft",(int)(100*FZL2));
    gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
    j0-=(brect[7]-brect[3]);
    gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);
  }
}

// shear 0-3 km
sprintf(string,"Shr 0-3 km=%i kt",ROUND(strtof(sh03, NULL)/0.514444444));
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);

// shear 0-6 km
sprintf(string,"Shr 0-6 km=%i kt",ROUND(strtof(sh06, NULL)/0.514444444));
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);

// shear 0-9 km
sprintf(string,"Shr 0-9 km=%i kt",ROUND(strtof(sh09, NULL)/0.514444444));
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);

// shear 1-3 km
sprintf(string,"Shr 1-3 km=%i kt",ROUND(strtof(sh13, NULL)/0.514444444));
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);

// shear 1-6 km
sprintf(string,"Shr 1-6 km=%i kt",ROUND(strtof(sh16, NULL)/0.514444444));
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);

// shear 1-9 km
sprintf(string,"Shr 1-9 km=%i kt",ROUND(strtof(sh19, NULL)/0.514444444));
gdImageStringFT(NULL,&brect[0],0,f,sz,0.,0,0,"Test9");
j0-=(brect[7]-brect[3]);
gdImageStringFT(image,&brect[0],black,f,sz,0.0,i0+3,j0,string);

/*Output of image to png-file. */

if ((fp=fopen(pngfile,"w"))==NULL) {
   printf("Output png-file could not be opened!\n");
   exit(5);
}
gdImagePng(image,fp);
fclose(fp);

/*End program.*/

exit(0);
}

/******************************************************************************/
/*This function converts the moist potential temperature and pressure to plot */
/*coordinates i and j. The potential temperature is given in Kelvin and the   */
/*pressure in hPa. The potential temperature and pressure are deformed using  */
/*a power function to get a balanced view of the troposphere and stratosphere,*/
/*i.e, to get a linear scale in geometric height, and to make the -20 degree  */
/*Celsius isotherm more or less linear. Subsequently, the deformed potential  */
/*temperature and pressure are scaled to the plot coordinates, i and j.       */
/******************************************************************************/
void thetas_p2plot(float theta_s,float pres,int *i,int *j) 
{
float xfrac,yfrac;
 extern int T_NCOLS,T_NROWS;
 extern float T_PRESN,T_PRESX,T_THETAN,T_THETAX;



xfrac=SIGN(theta_s-T_KTHETA)*pow(fabs(theta_s-T_KTHETA),T_KAPPA)-
	SIGN(T_THETAN-T_KTHETA)*pow(fabs(T_THETAN-T_KTHETA),T_KAPPA);
xfrac/=SIGN(T_THETAX-T_KTHETA)*pow(fabs(T_THETAX-T_KTHETA),T_KAPPA)-
	SIGN(T_THETAN-T_KTHETA)*pow(fabs(T_THETAN-T_KTHETA),T_KAPPA);
yfrac=pow(pres,T_KAPPA)-pow(T_PRESN,T_KAPPA);
yfrac/=pow(T_PRESX,T_KAPPA)-pow(T_PRESN,T_KAPPA);

*i=ROUND((0.25+0.7*xfrac)*T_NCOLS);
*j=ROUND((0.05+0.9*yfrac)*T_NROWS);
}

/******************************************************************************/
/*This function plots a windvane indicating the windspeed 'ff' and direction  */
/*'dd' at location (i,j) in an image using libgd. The windspeed should be     */
/*given in m/s and the direction in degrees of 'meteorological direction',    */
/*i.e., direction from which wind is coming.                                  */
/*The size and color of the windvanes are indicated by 'size' (in pixels) and */
/*'color' (color index allocated in 'image').                                 */
/******************************************************************************/
void barb2image(gdImagePtr image,int i,int j,float ff,float dd,int size,
								int color) 
{
int iff,N25,N5,F2,n;
float Gp,Lp,Ls,cosdd,sindd;
gdPoint points[3];

gdImageSetAntiAliased(image, color);

/*Setting sizes of gap between barbs 'Gp', and relative lengths of barbs */
/*parallel 'Lp' and perpendicular 'Ls' to windvane axis.*/

Gp=size/8;
Lp=size*cos(DEG2RAD*60)/2;
Ls=size*sin(DEG2RAD*60)/2;

/*Setting elements of rotation matrix for indicating wind direction.*/

cosdd=cos(DEG2RAD*dd);
sindd=sin(DEG2RAD*dd);

/*Calculation of the number of triangles (25m/s), the number of barbs */
/*(5m/s), and the flag for a half barb (2.5m/s).*/

iff=ROUND(ff/2.5);
N25=iff/10;
N5=(iff%10)/2;
F2=(iff%2);

/*When more than one triangle has to be drawn, the size of the windvane is */
/*increased.*/

if(N25>1) size+=(N25-1)*(Lp+Gp);

/*Drawing stick of windvane, or small circle when ff<5knots.*/

if (N25||N5||F2) gdImageLine(image,i,j,i+size*sindd,j-size*cosdd,gdAntiAliased);
else gdImageArc(image,i,j,size/20,size/20,0,360,gdAntiAliased);

/*Drawing the triangles, and adjusting position.*/

for (n=0 ; n<N25 ; n++) {
   points[0].x=i+size*sindd;
   points[0].y=j-size*cosdd;
   points[1].x=i+size*sindd+Ls*cosdd;
   points[1].y=j-size*cosdd+Ls*sindd;
   points[2].x=i+size*sindd-Lp*sindd;
   points[2].y=j-size*cosdd+Lp*cosdd;
   gdImageFilledPolygon(image,points,3,gdAntiAliased);
   size-=Gp+Lp;
}

/*Drawing the barbs, and adjusting position.*/

for (n=0 ; n<N5 ; n++) {
   gdImageLine(image,i+size*sindd,j-size*cosdd,i+size*sindd+Ls*cosdd+Lp*sindd,
					j-size*cosdd+Ls*sindd-Lp*cosdd,gdAntiAliased);
   size-=Gp;
}

/*Drawing the half barb.*/

if (F2) {
   Ls/=2;
   Lp/=2;
   gdImageLine(image,i+size*sindd,j-size*cosdd,i+size*sindd+Ls*cosdd+Lp*sindd,
					j-size*cosdd+Ls*sindd-Lp*cosdd,gdAntiAliased);
}
}

/******************************************************************************/
/*Latent heat of water condensation as a function of temperature. Constants in*/
/*function correspond to water vapor pressure function of Magnus.             */
/*Temperature in Kelvin and Latent Heat in J/kg.                              */
/******************************************************************************/
float LatHeat(float Temp)
{
return R_AIR/EPSILON*WV_CA*WV_CB*SQUARE(Temp/(Temp-CEL2KEL+WV_CB));
}

/******************************************************************************/
/*Saturation Pressure of Water Vapor as a function of temperature according   */
/*to Magnus.                                                                  */
/*Temperature is given in Kelvin and pressure in Pa.                          */
/******************************************************************************/
float e_sat(float Temp)
{
Temp-=CEL2KEL;
return WV_CE*exp(WV_CA*Temp/(Temp+WV_CB));
}

/******************************************************************************/
/*Dewpoint temperature from mixing ratio 'mixr' and environmental pressure    */
/*'pres'. Water Vapor Pressure function of Magnus is used. Mixing ratio is    */
/*given in kg/kg, pressure in Pa, and dewpoint temperature in Kelvin.         */
/******************************************************************************/
float mixr2Tdew(float mixr,float pres)
{
float e_vap;
if (mixr<1e-6) mixr=1e-6;
e_vap=pres*mixr/(EPSILON+mixr);
return WV_CB/(WV_CA/log(e_vap/WV_CE)-1.0)+CEL2KEL;
}

/******************************************************************************/
/*Mixing ratio from dewpoint temperature 'Tdew' and environmental pressure    */
/*'pres'. Water Vapor Pressure function of Magnus is used. Mixing ratio is    */
/*given in kg/kg, pressure in Pa, and dewpoint temperature in Kelvin.         */
/******************************************************************************/
float Tdew2mixr(float Tdew,float pres)
{
float e_vap;
Tdew-=CEL2KEL;
e_vap=WV_CE*exp(WV_CA*Tdew/(Tdew+WV_CB));
return EPSILON*e_vap/(pres-e_vap);
}

/******************************************************************************/
/*Wetbulb temperature from dewpoint temperature 'Tdew',environmental pressure */
/*'pres' and dry bulb temperature 'Temp'. Temperatures are in Kelvin, pressure*/
/*in Pascal. Uses LatHeat and e_sat functions.                                */
/******************************************************************************/
float Tdew2Twet(float Tdew,float Temp,float pres)
{
float edew,etemp,t_iter,c1,c2,Tw;
if (Temp==Tdew) return Temp;
c1=LatHeat(Temp)/461.51;
c2=1005.0/0.622/LatHeat(Temp)*pres;
edew=e_sat(Tdew);
etemp=e_sat(Temp);
t_iter=Temp-(1.0-edew/etemp)/0.19;
do {
   Tw=t_iter;
   c1=LatHeat(Tw)/461.41;
   etemp=e_sat(Tw);
   t_iter=Tw-(etemp-edew+c2*(Tw-Temp))/(c1*etemp/SQUARE(Tw)+c2);
}
while (abs(Tw - t_iter)>0.03);

return Tw;
}


/******************************************************************************/
/*Derivative of adiabatic temperature profile with respect to pressure for    */
/*dry air (mode: 'd') or saturated air (mode='s'). The derivative dT/dp is    */
/*given in K/Pa, pressure in Pa, and temperature in Kelvin.                   */
/******************************************************************************/
float dTempdp(float pres,float Temp,char mode) 
{
float e_s,r_s,Lv,dTdp;
e_s=e_sat(Temp);
r_s=EPSILON*e_s/(pres-e_s);
Lv=LatHeat(Temp);
dTdp=2.0*Temp/(7.0*pres);
if (mode=='S'||mode=='s') {
   dTdp*=1.0+r_s*Lv/(R_AIR*Temp);
   dTdp/=1.0+r_s/(7.0*EPSILON)+SQUARE(Lv/Temp)*r_s*EPSILON/(R_AIR*HEATCP);
}
return dTdp;
}

/******************************************************************************/
/*Temperature of adiabatic profile at pressure 'pres' for dry (mode: 'd') or  */
/*saturated (mode: 's') air. Temperature at pressure is calculated by         */
/*integration of derivative dT/dp from temperature 'Temp0' and pressure       */
/*'pres0'. Pressures are given in Pa and temperatures in Kelvin.              */
/*Integration is performed using the fourth-order Runge-Kutta method (see     */
/*Numerical Recipes, Chapter 16.1                                             */
/******************************************************************************/
float TempPres(float pres,float pres0,float Temp0,char mode)
{
int nstep,n;
float pstep=1000.0,pres_n,Temp_n,k1,k2,k3,k4;
if (pres<pres0) pstep=-pstep;
nstep=ROUND((pres-pres0)/pstep);
if (nstep<1) nstep=1;
pstep=(pres-pres0)/nstep;
Temp_n=Temp0;
pres_n=pres0;
for (n=0 ; n<nstep ; n++) {
   k1=pstep*dTempdp(pres_n,Temp_n,mode);
   k2=pstep*dTempdp(pres_n+0.5*pstep,Temp_n+0.5*k1,mode);
   k3=pstep*dTempdp(pres_n+0.5*pstep,Temp_n+0.5*k2,mode);
   k4=pstep*dTempdp(pres_n+pstep,Temp_n+k3,mode);
   Temp_n+=k1/6.0+k2/3.0+k3/3.0+k4/6.0;
   pres_n+=pstep;
}
return Temp_n;
}

/******************************************************************************/
/*Temperature of adiabatic profile at pressure 'pres' for dry air. Temperature*/
/*at pressure is calculated using the analytical formula and temperature      */
/*'Temp0' and pressure 'pres0'. Pressures are given in Pa and temperatures    */
/*in Kelvin.                                                                  */
/*Integration is performed using the fourth-order Runge-Kutta method (see     */
/*Numerical Recipes, Chapter 16.1                                             */
/******************************************************************************/
float TempPresDry(float pres,float pres0,float Temp0)
{
return Temp0*pow(pres/pres0,2.0/7.0);
}

/******************************************************************************/
/* Calculate FL from Pressure                                                 */
/******************************************************************************/
float pres2fl(float pres,float pressfc) {
  float T0,T1,g,R,P11,h,GAMMA;
  
  pressfc *= 100;
  T0=288.15;
  T1=216.65;
  g=9.80665;
  R=287.0528742;
  GAMMA=0.0065;
  P11=pressfc*exp(-g/GAMMA/R*log(T0/T1));
  
  if (pres>22632.04) 
    return(1/GAMMA*(T0-T0/exp(-GAMMA*R/g*log(pres/pressfc)))/30.48);
  else
    return( (11000 - R*T1/g*log(pres/22632.04))/30.48);
}

/******************************************************************************/
/* Calculate pressure from FL                                                 */
/******************************************************************************/
float fl2pres(float h,float P0) {
      float R,L0,L1,g0,T0,T1,M,P1,h1;
      R = 8.31432;
      M = 0.0289644;
      g0 = 9.80665;
      
      T0 = 288.15;
      L0 = -0.0065;
      P0 *= 100;
      
      T1 = 216.65;
      P1 = 22632.10;
      L1 = 0;
      h1 = 11000;
      
      if (h<11000){
         return P0*(pow((T0/(T0 + L0*(h-0))), ((g0*M)/(R*L0))));
      }
      else {
         return P1*exp((-g0*M*(h-h1))/(R*T1));
      }     
}

/******************************************************************************/
/* Calculate wet bulb from T/Td                                                 */
/******************************************************************************/
float calc_Tw(float T, float Td, float p){

	T -= 273.15;
	Td -= 273.15;
	p /= 100;
	float Tw;
	float right_eq_tmp;
	float left_eq = 6.112*exp((17.67*Td)/(Td+243.5));
	float right_eq = 1000000.;
	float wet_bulb= Td;
	
	for(Tw=Td; Tw<=T; Tw=Tw+0.01){
		right_eq_tmp = 6.112*exp((17.67*Tw)/(Tw+243.5))-(p*(1005./(0.622*2.5*1000000.))*(T-Tw));
		if(fabs(left_eq-right_eq_tmp)<fabs(left_eq-right_eq)){
			right_eq = right_eq_tmp;
		} else {
			wet_bulb=Tw;
			break;
		}
	}
	return wet_bulb+273.15;
}

/******************************************************************************/
/* bereken virtuele temperatuur op basis van de mixing ratio en de temperatuur*/
/* De mixing ratio wordt weer berekend met behulp van Td en de druk           */
/******************************************************************************/
float calc_virtual(float t, float td, float pres){

	return t*(1+ 0.61*Tdew2mixr(td, pres));
}

