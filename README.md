# Bijvoetdiagram



## Over het diagram
Op het KNMI gebruiken we het Bijvoetdiagram i.p.v. het wereldwijd meestal gebruikte Skew-T-diagram. Met name (on)stabiliteit kan je in dit diagram sneller zien. Dit diagram is ooit tijdens de tweede wereldoorlog bedacht door Herman Bijvoet, de latere hoofddirecteur van het KNMI.

## Over de software
Deze software plot de temperatuur, dauwpuntstemperatuur en windvaantjes op drukhoogtes. De originele code is geschreven door Iwan Holleman. Sybren de Haan voegde de windvaantjes toe. [Thom Zwagers](https://gitlab.com/tzwagers) voegde het hodogram toe, de natteboltemperatuur, de virtuele temperatuur en paste een aantal instellingen aan voor gebruik in de weerkamer. Deze zogenaamde progtemps worden gemaakt met modeldata van ECMWF en Harmonie.

## Compileren van de C-code
`make`

## Gebruik van de software
`./drawprogtemp.x input output.png -preserve -ncols 620 -nrows 800 -presn 15000. -presx 106000. -thetan 243.15 -thetax 313.15 -FZL`

input: CSV-bestand (eigenlijk TAB separated) waarvan de eerste 11 regels deze vorm hebben:
```
#Vertical Profile from ECMWF
#Profile run : 2022081200
#Profile station : Mechelen
#Profile valid : 15/18
#SFC Pressure: 1002.3723733511625
#SHR 0-3: 0
#SHR 0-6: 2
#SHR 0-9: 6
#SHR 1-3: 3
#SHR 1-6: 4
#SHR 1-9: 6
```
Daarna volgen regels met een aantal kolommmen:

`level     druk (Pa)   temperatuur (K)      dauwpunt (K)      ddd     ff       hoogte (vt)      Teq (K)      cld`

Waarvan de laatste 3 kolommen nog niet gebruikt worden




